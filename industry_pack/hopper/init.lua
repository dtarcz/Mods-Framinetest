-- Hopper mod for Minetest
-- Copyright 2012 Mark Holmquist <mtraceur@member.fsf.org>
--
-- The mechanical arm mod is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- The mechanical arm mod is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with the mechanical arm mod. If not, see <http://www.gnu.org/licenses/>.

-- Boilerplate to support localized strings if intllib mod is installed.
local S
if (minetest.get_modpath("intllib")) then
  dofile(minetest.get_modpath("intllib").."/intllib.lua")
  S = intllib.Getter(minetest.get_current_modname())
else
  S = function ( s ) return s end
end

minetest.register_craftitem( 'hopper:hopper', {
    description = S('Hopper'),
    inventory_image = 'hopper.png',
} )

minetest.register_craft( {
    output = 'hopper:hopper',
    recipe = {
        { 'default:steel_ingot', 'rubber_sheet:rubber_sheet', 'default:steel_ingot' },
        { 'default:steel_ingot', 'rubber_sheet:rubber_sheet', 'default:steel_ingot' },
        { 'default:steel_ingot', 'default:steel_ingot', 'default:steel_ingot' }
    }
} )

