# French translation for Animals Modpack.
# Traduction en Français d’Animals Modpack.
# Author/Auteur: Nicolas « Powi » Denis

Sword (Deamondeath) = Epée (Mortdémon)
Scissors = Ciseaux
Lasso = Lasso
Net = Filet
Saddle = Selle
Contract = Contrat
Raw meat = Viande crue
Pork (raw) = Porc (cru)
Beef (raw) = Bœuf (cru)
Chicken (raw) = Poulet (cru)
Lamb (raw) = Agneau (cru)
Venison (raw) = Venaison (cru)
Meat (not quite dead) = Viande (pas totalement morte)
Toxic Meat = Viande Toxique
Ostrich Meat = Viande d’Autruche
Pork = Porc
Fish (bluewhite) = Poisson (bleu-blanc)
Fish (clownfish) = Poisson (poisson clown)
Feather = Plume
Milk = Lait
Egg = Œuf
Egg (big) = Œuf (gros)
Bone = Os
Fur = Fourrure
Deer fur = Fourrure de Cervidé 
Cattle coat = Cuir bovin
Deer horns = Bois de cerf
Ivory = Ivoire
Scale (golden) = Ecaille (dorée)
Scale (white) = Ecaile (blanche)
Scale (grey) = Ecaille (gris)
Scale (blue) = Ecaille (bleu)
